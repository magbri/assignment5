<html>
   <body>
      
      <?php
		class Hoved{
			public $dom;
			public $db;
			public $xpath;
			
			public function __construct(){
				$this->db = new PDO('mysql:host = localhost:8080; dbname=olbig5; charset = utf8', 'root', '');
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
				$this->dom = new DOMDocument();
				$this->dom->load('hoved/SkierLogs.xml');
				
				$this->xpath = new DOMXPath($this->dom);
			}
			
			public function master(){
				$this->getSeason();
				$this->getClubs();
				$this->getSkiers();
				$this->getDistances();
            
            
				//echo "test";
			}
			
			public function getSkiers(){
				$arr1=array();
				$arr2=array();
				$arr3=array();
				$arr4=array();
				$userName	= $this->dom->getElementsByTagName("Skier");
				$firstName	= $this->dom->getElementsByTagName("FirstName");
				$lastName	= $this->dom->getElementsByTagName("LastName");
				$YOB		= $this->dom->getElementsByTagName("YearOfBirth");
				foreach($userName as $u){
					array_push($arr1, $u->getAttribute("userName"));
				}
				foreach($firstName as $f){
					array_push($arr2, $f->nodeValue);
				}
				foreach($lastName as $l){
					array_push($arr3, $l->nodeValue);
				}
				foreach($YOB as $y){
					array_push($arr4, $y->nodeValue);
				}
				$arr1 = array_unique($arr1);
				for($x = 0; $x < sizeOf($arr1); $x++){
					try {
						$input = $this->db->prepare("INSERT INTO skier(userName, firstName, lastName, YearOfBirth) VALUES (:userName, :firstName, :lastName, :yearOfBirth)");
						$input->bindValue(':userName',		$arr1[$x], PDO::PARAM_STR);
						$input->bindValue(':firstName', 	$arr2[$x], PDO::PARAM_STR);
						$input->bindValue(':lastName', 		$arr3[$x], PDO::PARAM_STR);
						$input->bindValue(':yearOfBirth', 	$arr4[$x], PDO::PARAM_STR);
						$input->execute();
					}
					catch(Exception $e){
						echo "feil";
						echo $e->getMessage();
					}
				}
			}
			public function getClubs(){
				$arr1=array();
				$arr2=array();
				$arr3=array();
				$arr4=array();
				$id		= $this->dom->getElementsByTagName("Club");
				$name	= $this->dom->getElementsByTagName("Name");
				$city	= $this->dom->getElementsByTagName("City");
				$county	= $this->dom->getElementsByTagName("County");
				foreach($id as $i){
					array_push($arr1, $i->getAttribute("id"));
				}
				foreach($name as $n){
					array_push($arr2, $n->nodeValue);
				}
				foreach($city as $ci){
					array_push($arr3, $ci->nodeValue);
				}
				foreach($county as $co){
					array_push($arr4, $co->nodeValue);
				}
				for($x = 0; $x < sizeOf($arr1); $x++){
					try {
						$input = $this->db->prepare("INSERT INTO clubs(Id, clubName, city, county) VALUES (:id, :name, :city, :county)");
						$input->bindValue(':id',		$arr1[$x], PDO::PARAM_STR);
						$input->bindValue(':name',		$arr2[$x], PDO::PARAM_STR);
						$input->bindValue(':city',		$arr3[$x], PDO::PARAM_STR);
						$input->bindValue(':county',	$arr4[$x], PDO::PARAM_STR);
						$input->execute();
					}
					catch(Exception $e){
						echo "feil";
						echo $e->getMessage();
					}
				}
			}
			public function getSeason(){
				$arr1=array();
				$fallYear = $this->dom->getElementsByTagName("Season");
				foreach($fallYear as $f){
					array_push($arr1, $f->getAttribute("fallYear"));
				}
				for($x = 0; $x < sizeOf($arr1); $x++){
					try{
						$input = $this->db->prepare("INSERT INTO season(fallYear) VALUES (:fallYear)");
						$input->bindValue(':fallYear', $arr1[$x], PDO::PARAM_STR);
						$input->execute();
					}
					catch(Exception $e){
						echo "feil";
						echo $e->getMessage();
					}
				}
			}
			public function getDistances(){
				$sum = 0;
				$participatingUserNames = array();
				$participatingClubs = array();
				$participatingDistance = array();
				$tempNames = $this->xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier/@userName');
				foreach($tempNames as $t){
					array_push($participatingUserNames, trim($t->nodeValue));
				}
				for($i = 0; $i<sizeOf($participatingUserNames); $i++){
					$tempClubs = $this->xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$participatingUserNames[$i].'"]/../@clubId');
					foreach($tempClubs as $t){
						if($t)
							array_push($participatingClubs, trim($t->nodeValue));
						else
							echo'hei';
					}
				}
				for($i = 0; $i<sizeOf($participatingUserNames); $i++){
					$distance = $this->xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$participatingUserNames[$i].'"]/Log/Entry/Distance');
					for($j=0; $j<$distance->length; $j++){
						$sum+= ($distance[$j]->nodeValue);
					}
					array_push($participatingDistance, $sum);
					$sum = 0;
				}
				for($x = 0; $x < sizeOf($participatingUserNames); $x++){
					try {
						$input = $this->db->prepare("INSERT INTO totaldistance(clubId, skierName, seasonYear, totalDistance) VALUES (:id, :name, :year, :distance)");
						$input->bindValue(':id',		$participatingClubs[$x],		PDO::PARAM_STR);
						$input->bindValue(':name',		$participatingUserNames[$x], 	PDO::PARAM_STR);
						$input->bindValue(':year',		'2015', 						PDO::PARAM_STR);
						$input->bindValue(':distance',	$participatingDistance[$x], 	PDO::PARAM_STR);
						$input->execute();
					}
					catch(Exception $e){
						echo "feil";
						echo $e->getMessage();
					}
				}
				unset($participatingUserNames);
				$participatingUserNames = array();
				unset($participatingClubs);
				$participatingClubs = array();
				unset($participatingDistance);
				$participatingDistance = array();
				unset($tempNames);
				$tempNames = array();
				$tempNames = $this->xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier/@userName');
				foreach($tempNames as $t){
					array_push($participatingUserNames, trim($t->nodeValue));
				}
				for($i = 0; $i<sizeOf($participatingUserNames); $i++){
					$tempClubs = $this->xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$participatingUserNames[$i].'"]/../@clubId');
					foreach($tempClubs as $t){
						if(!empty(trim($t->nodeValue))){
							array_push($participatingClubs, trim($t->nodeValue));
						}
						else
							echo 'utenklubb';
					}
				}
				for($i = 0; $i<sizeOf($participatingUserNames); $i++){
					$distance = $this->xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$participatingUserNames[$i].'"]/Log/Entry/Distance');
					for($j=0; $j<$distance->length; $j++){
						$sum+= ($distance[$j]->nodeValue);
					}
					array_push($participatingDistance, $sum);
					$sum = 0;
				}
				for($x = 0; $x < sizeOf($participatingUserNames); $x++){
					try {
						$input = $this->db->prepare("INSERT INTO totaldistance(clubId, skierName, seasonYear, totalDistance) VALUES (:id, :name, :year, :distance)");
						$input->bindValue(':id',		$participatingClubs[$x],		PDO::PARAM_STR);
						$input->bindValue(':name',		$participatingUserNames[$x], 	PDO::PARAM_STR);
						$input->bindValue(':year',		'2016', 						PDO::PARAM_STR);
						$input->bindValue(':distance',	$participatingDistance[$x], 	PDO::PARAM_STR);
						$input->execute();
					}
					catch(Exception $e){
						echo "feil";
						echo $e->getMessage();
					}
				}
				}
			}
		}
      ?>
		
   </body>
</html>