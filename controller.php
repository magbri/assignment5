<?php
	require_once("hoved/hoved.php");
	class Controller{
		protected $hoved = null;
		public function __construct(){
			$this->hoved = new hoved();
		}
		public function invoke(){
			$this->hoved->master();
		}
	}
?>